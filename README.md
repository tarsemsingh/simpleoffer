# SimpleOffer

[![License](http://img.shields.io/:license-apache-blue.svg)](http://www.apache.org/licenses/LICENSE-2.0.html)

SimpleOffer is a [Spring Boot](http://projects.spring.io/spring-boot/) RESTful application.
The application provides a RESTful API, which allows to create a new `Offer`. The `Offer`, once created, can be queried.
The `Offer` expires after a period of time defined on it and further requests to query the expired offer result in a response that offer is not available.
The API also allows to cancel the offer before its expiry.

## Requirements

For building and running the application you need:

- [JDK 1.8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
- [Maven 3](https://maven.apache.org)

## Running the application locally


You can use the [Spring Boot Maven plugin](https://docs.spring.io/spring-boot/docs/current/reference/html/build-tool-plugins-maven-plugin.html) to run the application:

```shell
mvn spring-boot:run
```

## Run the Unit and Integration tests

```shell
mvn test
```

# SimpleOffer RESTful API

The application uses `Product` and `Offer` as domain objects.
The RESTful API for the `SimpleOffer` app is described below.

## Get list of Products

### Request

`GET /products/`

    curl -i -H 'Accept: application/json' http://localhost:8080/simpleoffer/products

### Response

    HTTP/1.1 200
    Content-Type: application/json;charset=UTF-8
    Transfer-Encoding: chunked
    Date: Mon, 18 Jun 2018 15:09:58 GMT
    []

## Create a new Product

### Request

`POST /products/`

    curl -i -H 'Content-Type: application/json' -d '{"name":"Product 1","description":"Dummy product 1","price":{"amount":12.5,"currency":"GBP"}}' http://localhost:8080/simpleoffer/products

### Response

    HTTP/1.1 201
    Content-Type: application/json;charset=UTF-8
    Transfer-Encoding: chunked
    Date: Mon, 18 Jun 2018 15:15:11 GMT

    {"productId":1,"name":"Product 1","description":"Dummy product 1","price":{"amount":12.5,"currency":"GBP"}}

## Create a new Offer

### Request

`POST /offers/`

    curl -i -H 'Content-Type: application/json' -d '{"description":"Demo offer","price":{"amount":11.5,"currency":"GBP"},"startDate":"2018-06-01T00:00:00.000+0000","expirationDate":"2018-08-21T00:00:00.000+0000","availability":"Available","product":{"productId":1}}' http://localhost:8080/simpleoffer/offers

### Response

    HTTP/1.1 201
    Content-Type: application/json;charset=UTF-8
    Transfer-Encoding: chunked
    Date: Mon, 18 Jun 2018 15:24:33 GMT

    {"id":4,"description":"Demo offer","price":{"amount":11.5,"currency":"GBP"},"startDate":"2018-06-01T00:00:00.000+0000","expirationDate":"2018-08-21T00:00:00.000+0000","availability":"Available","product":{"productId":1,"name":null,"description":null,"price":null}}

## Get list of Offers

### Request

`GET /offers/`

    curl -i -H 'Accept: application/json' http://localhost:8080/simpleoffer/offers

### Response

    HTTP/1.1 200
    Content-Type: application/json;charset=UTF-8
    Transfer-Encoding: chunked
    Date: Mon, 18 Jun 2018 15:26:04 GMT


    [{"id":3,"description":"Demo offer","price":{"amount":11.5,"currency":"GBP"},"startDate":"2018-06-01T00:00:00.000+0000","expirationDate":"2018-08-21T00:00:00.000+0000","availability":"Available","product":{"productId":1,"name":"Product 1","description":"Dummy product 1","price":{"amount":12.5,"currency":"GBP"}}},{"id":4,"description":"Demo offer","price":{"amount":11.5,"currency":"GBP"},"startDate":"2018-06-01T00:00:00.000+0000","expirationDate":"2018-08-21T00:00:00.000+0000","availability":"Available","product":{"productId":1,"name":"Product 1","description":"Dummy product 1","price":{"amount":12.5,"currency":"GBP"}}}]

# Get a specific Offer

### Request

`GET /offers/id`

    curl -i -H 'Accept: application/json' http://localhost:8080/simpleoffer/offers/1

### Response

    HTTP/1.1 200
    Content-Type: application/json;charset=UTF-8
    Transfer-Encoding: chunked
    Date: Mon, 18 Jun 2018 15:28:57 GMT

    {"id":1,"description":"Demo offer","price":{"amount":11.5,"currency":"GBP"},"startDate":"2018-06-01T00:00:00.000+0000","expirationDate":"2018-08-21T00:00:00.000+0000","availability":"Available","product":{"productId":1,"name":"Product 1","description":"Dummy product 1","price":{"amount":12.5,"currency":"GBP"}}}

## Cancel a Offer

### Request

`PUT /offers/:id?availability=cancelled`

    curl -i -H 'Accept: application/json' -X PUT http://localhost:8080/simpleoffer/offers/1?availability=cancelled

### Response

    HTTP/1.1 200
    Location: http://localhost:8080/offers/1
    Content-Length: 0
    Date: Mon, 18 Jun 2018 15:36:15 GMT


## Copyright

Released under the Apache License 2.0. See the [LICENSE](http://www.apache.org/licenses/LICENSE-2.0.txt) file.
