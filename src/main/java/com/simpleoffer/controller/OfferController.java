/*
 * Copyright 2011-2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.simpleoffer.controller;

import com.simpleoffer.model.Offer;
import com.simpleoffer.service.OfferService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

@RestController
@RequestMapping("/simpleoffer/offers")
public final class OfferController {
    private static final Logger logger = LoggerFactory.getLogger(OfferController.class);
    @Autowired
    private  OfferService offerService;

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<Offer> offers() {
        return offerService.getAllOffers();
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Offer offer(@PathVariable Integer id ) {
        logger.info("Retrieving the offer: " + id);
        return offerService.getOffer(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    Offer create(@RequestBody Offer offer) {
        logger.info("Create offer: " + offer);
        return offerService.createOffer(offer);
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    ResponseEntity<?> update(@PathVariable Integer id, @RequestParam("availability") String availability, UriComponentsBuilder ucb) {
        logger.info("Update the offer: " + id);
        if(availability.equalsIgnoreCase("cancelled")) {
            offerService.cancelOffer(id);
        } else {
            logger.warn("Invalid request parameter.");
        }
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucb.path("/offers/{id}").buildAndExpand(id).toUri());
        return new ResponseEntity<>(headers, HttpStatus.OK);
    }
    //NOTE: I am intentionally leaving the DELETE operation
    //@DeleteMapping("/offers/{id}")
    //Offer delete(@PathVariable Integer id) {
    //    logger.info("Delete the offer: " + id);
        //TODO:
    //    return offer;
    //}
}
