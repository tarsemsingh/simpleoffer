/*
 * Copyright 2011-2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.simpleoffer.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="offers")
public final class Offer {
    @Id
    @GeneratedValue
    private Integer id; // Offer id, it is auto generated
    @Column(nullable = false)
    private String description;  // offer description
    @Embedded
    private Price price;  // Offer price
    @Column(nullable = false)
    private Date startDate;       // assumption offer start at mid-night
    @Column(nullable = false)
    private Date expirationDate; // assumption offer expire at mid-night
    @Column(nullable = true)
    private Availability availability;  // by default the offer is un-available

    @OneToOne( fetch = FetchType.LAZY)
    @JoinColumn(name = "product_id")
    private Product product;  // Product on which the offer is applied. For simplicity assume OneToOne relationship

    /**
     * Default constructor to initialise the offer to Available state
     */
    public Offer() { this.availability = Availability.Available; }

    /**
     * Overloaded constructor to initialise the offer with specified input parameters
     * @param description
     * @param price
     */
    public Offer(String description, Price price) {
        this.description = description;
        this.price = price;
        this.availability = Availability.Available;
    }

    /**
     * Overloaded constructor to initialise the offer with specified input parameters
     * @param description
     * @param price
     * @param startDate
     * @param expirationDate
     * @param availability
     */
    public Offer(String description, Price price, Date startDate, Date expirationDate, Availability availability) {
        this.description = description;
        this.price = price;
        this.startDate = startDate;
        this.expirationDate = expirationDate;
        this.availability = availability;
    }

    /**
     * Get the offer id
     * @return id of the offer
     */
    public Integer getId() {
        return id;
    }

    /**
     * Set the id of the offer
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Get the description of the Offer
     * @return String description of the Offer
     */
    public String getDescription() {
        return description;
    }

    /**
     * Set the description of the Offer
     * @param description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Set the Price of the Offer
     * @param price
     */
    public void setPrice(Price price) {
        this.price = price;
    }

    /**
     * Get the Offer price
     * @return Price of the Offer
     */
    public Price getPrice() {
        return price;
    }

    /**
     * Get start date of the Offer
     * @return Date
     */
    public Date getStartDate() {
        return startDate;
    }

    /**
     * Set the start date of the Offer
     * @param startDate
     */
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    /**
     * Get the expiration date of the Offer
     * @return Date
     */
    public Date getExpirationDate() {
        return expirationDate;
    }

    /**
     * Set the expiration date of the Offer
     * @param expirationDate
     */
    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    /**
     * Get the availability of the Offer
     * @return Availability
     */
    public Availability getAvailability() {
        return availability;
    }

    /**
     * Set the availability of the Offer
     * @param availability
     */
    public void setAvailability(Availability availability) {
        this.availability = availability;
    }

    /**
     * Get the offered Product
     * @return Product or the null if no product is associated with the Offer
     */
    public Product getProduct() {
        return product;
    }

    /**
     * Set the Product for the Offer
     * @param product
     */
    public void setProduct(Product product) {
        this.product = product;
    }

    @Override
    public String toString() {
        return String.format(
                "Offer[id=%d, description='%s']",
                id, description);
    }
}
