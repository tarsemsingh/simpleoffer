/*
 * Copyright 2011-2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.simpleoffer.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.util.Currency;

@Embeddable
public final class Price {
    @Column(nullable = false)
    private Float amount;
    @Column(nullable = false)
    private Currency currency;

    /**
     * Default constructor for initialisation
     */
    public Price() {}
    /**
     * Overloaded constructor initiliase the instance with the specified input parameters
     */
    public Price(Currency currency, Float amount) {
        this.currency = currency;
        this.amount = amount;
    }

    /**
     * Get the currency for the Price
     * @return Currency
     */
    public Currency getCurrency() {
        return currency;
    }

    /**
     * Set currency of the Price
     * @param currency
     */
    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    /**
     * Get amount of the Price
     * @return Float amount
     */
    public Float getAmount() {
        return amount;
    }

    /**
     * Set amount of the Price instance
     * @param amount
     */
    public void setAmount(Float amount) {
        this.amount = amount;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null) {
            return false;
        }
        if(this == obj) {
            return true;
        }
        if(obj.getClass() != this.getClass() ) {
            return false;
        }
        return ( currency.equals(((Price)obj).currency) && amount.equals(((Price)obj).amount));
    }

    @Override
    public String toString() {
        return String.format(
                "Price[amount=%d, currency='%s']",
                amount, currency);
    }
}
