/*
 * Copyright 2011-2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.simpleoffer.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
public final class Product {
    @Id
    @GeneratedValue( strategy = GenerationType.AUTO)
    private Integer productId;
    @Column(nullable = false)
    private String name;
    @Column(nullable = false)
    private String description;
    @Embedded
    @NotNull
    private Price price;

    public Product() {}

    public Product(String name, String description, Price price) {
        this.name = name;
        this.description = description;
        this.price = price;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Price getPrice() {
        return price;
    }

    public void setPrice(Price price) {
        this.price = price;
    }

    @Override
    public String toString() {
        String result = String.format(
                "Product[productId=%d, name='%s']%n",
                productId, name);
        return result;
    }
}
