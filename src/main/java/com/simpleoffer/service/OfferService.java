/*
 * Copyright 2011-2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.simpleoffer.service;

import com.simpleoffer.model.Offer;

import java.util.List;

/**
 * Service interface to create, get and update the Offer from the storage
 */
public interface OfferService {
    /**
     * This method is creates a new Offer and store it in the database
     * @param offer
     * @return the created offer
     */
    public Offer createOffer(Offer offer);

    /**
     * This method get all the offers from the database
     * @return List of the offers
     */
    public List<Offer> getAllOffers();

    /**
     * Get the offer for the specified id
     * @param id if the offer
     * @return Offer if found in the storage
     */
    public Offer getOffer(Integer id);

    /**
     * Cancel the offer correponds to the given id
     * @param id of the offer
     * @return The cancelled offer
     */
    public Offer cancelOffer(Integer id);
}
