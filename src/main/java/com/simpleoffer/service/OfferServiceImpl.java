/*
 * Copyright 2011-2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.simpleoffer.service;

import com.simpleoffer.exception.InvalidRequestException;
import com.simpleoffer.exception.ResourceNotFoundException;
import com.simpleoffer.model.Availability;
import com.simpleoffer.model.Offer;
import com.simpleoffer.repository.OfferRepository;
import com.simpleoffer.repository.ProductRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by tarsem on 16/06/2018.
 */
@Service
public final class OfferServiceImpl implements OfferService {
    private static final Logger logger = LoggerFactory.getLogger(OfferServiceImpl.class);
    private final OfferRepository offerRepository;
    private final ProductRepository productRepository;

    public OfferServiceImpl(OfferRepository offerRepository, ProductRepository productRepository) {
        this.offerRepository = offerRepository;
        this.productRepository = productRepository;
    }

    @Override
    public Offer createOffer(Offer offer) {
        try {
            return offerRepository.saveAndFlush(offer);
        } catch( Exception ex ) {
            logger.error("Create offer: " + offer);
            throw new InvalidRequestException(offer.toString());
        }
    }

    @Override
    public List<Offer> getAllOffers() {
        return offerRepository.findAllByAvailability();
    }

    @Override
    public Offer getOffer(Integer id) {
        return offerRepository.findIdByAvailability(id).orElseThrow(() -> new ResourceNotFoundException(id));
    }

    @Override
    public Offer cancelOffer(Integer id) {
        Offer offer = offerRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException(id));
        offer.setAvailability(Availability.Cancelled);
        offerRepository.save(offer);
        return offer;
    }
}
