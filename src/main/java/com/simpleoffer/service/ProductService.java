/*
 * Copyright 2011-2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.simpleoffer.service;

import com.simpleoffer.model.Product;

import java.util.List;

/**
 * Service interface to create and get the Products
 */
public interface ProductService {
    /**
     * This method create a new Product in the database
     * @param product
     * @return The the newly created Product
     */
    Product create(Product product);

    /**
     * This method gets all the Products stored in the database
     * @return List of the Products
     */
    List<Product> getAll();
}
