package com.simpleoffer;

/**
 * Created by
 */

import com.simpleoffer.model.Offer;
import com.simpleoffer.model.Price;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Currency;
import java.util.Date;

import static org.apache.commons.lang3.RandomStringUtils.randomNumeric;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SimpleOfferApplication.class, webEnvironment = WebEnvironment.DEFINED_PORT)
public class OfferControllerIntegrationTests {

    private static final String API_ROOT = "http://localhost:8080/simpleoffer";

    @Test
    public void whenGetAllOffers_thenOK() {
        RestAssured.given()
                .contentType(ContentType.JSON)
                .when()
                .get(API_ROOT+"/offers")
                .then()
                .statusCode(HttpStatus.OK.value());
    }

    @Test
    public void whenGetOfferById_thenOK() {
        final Offer offer = createDummyOffer();
        Offer newOffer = createOfferAsUri(offer);
        RestAssured.given()
                .contentType(ContentType.JSON)
                .when()
                .pathParam("id", newOffer.getId())
                .get(API_ROOT+"/offers/{id}")
                .then()
                .statusCode(HttpStatus.OK.value());
    }

    @Test
    public void whenGetNotExistOfferById_thenNotFound() {
        RestAssured.given()
                .contentType(ContentType.JSON)
                .when()
                .pathParam("id", randomNumeric(4))
                .get(API_ROOT + "/offers/{id}")
                .then()
                .statusCode(HttpStatus.NOT_FOUND.value());
    }

    // POST
    @Test
    public void whenCreateNewOffer_thenCreated() {
        final Offer offer = createDummyOffer();
        RestAssured.given()
                .contentType(ContentType.JSON)
                .body(offer)
                .when()
                .post(API_ROOT + "/offers")
                .then()
                .statusCode(HttpStatus.CREATED.value());
    }

    @Test
    public void whenCreateInvalidOffer_thenError() {
        final Offer offer = createDummyOffer();
        offer.setDescription(null);
        RestAssured.given()
                .contentType(ContentType.JSON)
                .body(offer)
                .when()
                .post(API_ROOT + "/offers")
                .then()
                .statusCode(HttpStatus.BAD_REQUEST.value());
    }

    @Test
    public void whenCancelledOffer_thenNotFound() {
        final Offer offer = createDummyOffer();
        Offer newOffer = createOfferAsUri(offer);
        RestAssured.given()
                .contentType(ContentType.JSON)
                .body(offer)
                .when()
                .pathParam("id", newOffer.getId())
                .queryParam("availability", "cancelled")
                .put(API_ROOT + "/offers/{id}")
                .then()
                .statusCode(HttpStatus.OK.value());

        RestAssured.given()
                .when()
                .pathParam("id", newOffer.getId() )
                .get(API_ROOT + "/offers/{id}")
                .then()
                .statusCode(HttpStatus.NOT_FOUND.value());
    }

    // ===============Utility functions================

    private Offer createDummyOffer() {
        final Offer offer = new Offer();
        offer.setDescription("Test Offer");
        offer.setPrice(new Price(Currency.getInstance("GBP"), 12.5F));
        offer.setStartDate(Date.from(LocalDateTime.now().minusDays(1).toInstant(ZoneOffset.UTC)));
        offer.setExpirationDate(Date.from(LocalDateTime.now().plusDays(10).toInstant(ZoneOffset.UTC)));
        return offer;
    }

    private Offer createOfferAsUri(Offer offer) {
        final Response response = RestAssured.given()
                .contentType(ContentType.JSON)
                .body(offer)
                .post(API_ROOT + "/offers");
        return response.body().as(Offer.class);
    }

}