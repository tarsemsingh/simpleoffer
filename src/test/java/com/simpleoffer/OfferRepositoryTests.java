package com.simpleoffer;

import com.simpleoffer.model.Availability;
import com.simpleoffer.model.Offer;
import com.simpleoffer.model.Price;
import com.simpleoffer.repository.OfferRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Date;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Currency;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * Created by
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class OfferRepositoryTests {
    final int OFFERS_COUNT = 5;
    @Autowired
    OfferRepository repository;

    @Before
    public void setUp() {
        repository.deleteAll();
        for(int i = 0; i< OFFERS_COUNT; i++) {
            Offer offer = new Offer("Test Offer", new Price(Currency.getInstance("GBP"), 20.5F * i ));
            offer.setStartDate(Date.from(LocalDateTime.now().minusDays(1).toInstant(ZoneOffset.UTC)));
            offer.setExpirationDate(Date.from(LocalDateTime.now().plusDays(10).toInstant(ZoneOffset.UTC)));
            repository.save(offer);
        }
    }

    @Test
    public void testSaveOffer() throws Exception {
        assertThat(repository.findAll().size(), is(OFFERS_COUNT));
    }

    @Test
    public void testUpdateOfferAndFindByAvailability() throws Exception {
        assertThat(repository.findAllByAvailability().size(), is(OFFERS_COUNT));
        for(Offer offer: repository.findAll()) {
            offer.setAvailability(Availability.Cancelled);
        }
        assertThat(repository.findAllByAvailability().size(), is(0));
    }

    @After
    public void tearDown() {
        repository.deleteAll();
    }

}
