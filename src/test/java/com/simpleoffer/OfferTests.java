package com.simpleoffer;

import com.simpleoffer.model.Availability;
import com.simpleoffer.model.Offer;
import com.simpleoffer.model.Price;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Currency;
import java.util.Date;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * Created by
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class OfferTests {
    private Offer offer;
    private Price price;
    private Date startDate;
    private Date expirationDate;

    @Before
    public void setUp() {
        price = new Price(Currency.getInstance("GBP"), 20.5F );
        startDate = Date.from(LocalDateTime.now().minusDays(1).toInstant(ZoneOffset.UTC));
        expirationDate = Date.from(LocalDateTime.now().plusDays(10).toInstant(ZoneOffset.UTC));
        offer = new Offer("Test Offer", price);
        offer.setStartDate(startDate);
        offer.setExpirationDate(expirationDate);
    }

    @Test
    public void testSetId() throws Exception {
        offer.setId(10);
        assertThat(offer.getId(), is(10));
    }

    @Test
    public void testSetDescription(){
        offer.setDescription("New Offer Description");
        assertThat(offer.getDescription(), is("New Offer Description"));
    }

    @Test
    public void testSetPrice(){
        assertThat(offer.getPrice(), is(price));
        final Price price = new Price(Currency.getInstance("USD"), 500F );
        offer.setPrice(price);
        assertThat(offer.getPrice(), is(price));
    }

    @Test
    public void testSetStartDate(){
        assertThat(offer.getStartDate(), is(startDate));
        final Date date = Date.from(LocalDateTime.now().minusDays(20).toInstant(ZoneOffset.UTC));
        offer.setStartDate(date);
        assertThat(offer.getStartDate(), is(date));
    }

    @Test
    public void testSetExpirationDate(){
        assertThat(offer.getExpirationDate(), is(expirationDate));
        final Date date = Date.from(LocalDateTime.now().plusDays(20).toInstant(ZoneOffset.UTC));
        offer.setExpirationDate(date);
        assertThat(offer.getExpirationDate(), is(date));
    }

    @Test
    public void testSetAvailability(){
        assertThat(offer.getAvailability(), is(Availability.Available));
        offer.setAvailability(Availability.Cancelled);
        assertThat(offer.getAvailability(), is(Availability.Cancelled));
    }
}
