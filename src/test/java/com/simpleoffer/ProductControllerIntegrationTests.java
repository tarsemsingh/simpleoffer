package com.simpleoffer;

import com.simpleoffer.model.Price;
import com.simpleoffer.model.Product;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Currency;

/**
 * Created by
 */

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SimpleOfferApplication.class, webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class ProductControllerIntegrationTests {

    private static final String API_ROOT = "http://localhost:8080/simpleoffer";

    @Test
    public void whenGetAllProducts_thenOK() {
        RestAssured.given()
                .contentType(ContentType.JSON)
                .when()
                .get(API_ROOT+"/products")
                .then()
                .statusCode(HttpStatus.OK.value());
    }

    // POST
    @Test
    public void whenCreateNewProduct_thenCreated() {
        final Product product = createTestProduct();
        RestAssured.given()
                .contentType(ContentType.JSON)
                .body(product)
                .when()
                .post(API_ROOT + "/products")
                .then()
                .statusCode(HttpStatus.CREATED.value());
    }

    @Test
    public void whenCreateInvalidProduct_thenError() {
        final Product product = createTestProduct();
        product.setDescription(null);
        RestAssured.given()
                .contentType(ContentType.JSON)
                .body(product)
                .when()
                .post(API_ROOT + "/products")
                .then()
                .statusCode(HttpStatus.BAD_REQUEST.value());
    }

    // ===============Utility functions================

    private Product createTestProduct() {
        final Product product = new Product();
        product.setName("Test Product");
        product.setDescription("Product for integration testing.");
        product.setPrice(new Price(Currency.getInstance("GBP"), 20.12F));
        return product;
    }

    private Product createProductAsUri(Product product) {
        final Response response = RestAssured.given()
                .contentType(ContentType.JSON)
                .body(product)
                .post(API_ROOT + "/products");
        return response.body().as(Product.class);
    }

}