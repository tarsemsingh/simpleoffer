package com.simpleoffer;

import com.simpleoffer.model.Price;
import com.simpleoffer.model.Product;
import com.simpleoffer.repository.ProductRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.Currency;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * Created by
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class ProductRepositoryTests {
    final int PRODUCTS_COUNT = 10;
    @Autowired
    ProductRepository repository;

    @Before
    public void setUp() {
        repository.deleteAll();
        for(int i=0; i<PRODUCTS_COUNT; i++) {
            Product product = new Product("Test Product", "Product for testing", new Price(Currency.getInstance("GBP"), 20.5F * i));
            repository.save(product);
        }
    }

    @Test
    public void testSaveProduct() throws Exception {
        assertThat(repository.findAll().size(), is(PRODUCTS_COUNT));
    }

    @After
    public void tearDown() {
        repository.deleteAll();
    }
}
