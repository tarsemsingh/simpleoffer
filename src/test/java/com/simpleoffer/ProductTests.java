package com.simpleoffer;

import com.simpleoffer.model.Price;
import com.simpleoffer.model.Product;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Currency;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * Created by
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductTests {
    private Product product;
    private Price price;

    @Before
    public void setUp() {
        price = new Price(Currency.getInstance("GBP"), 20.5F );
        product = new Product("Test Product", "Product for unit testing", price);
    }

    @Test
    public void testSetId() throws Exception {
        product.setProductId(10);
        assertThat(product.getProductId(), is(10));
    }

    @Test
    public void testSetDescription(){
        product.setDescription("New Product Description");
        assertThat(product.getDescription(), is("New Product Description"));
    }

    @Test
    public void testSetPrice(){
        assertThat(product.getPrice(), is(price));
        final Price price = new Price(Currency.getInstance("USD"), 500F );
        product.setPrice(price);
        assertThat(product.getPrice(), is(price));
    }
}
