package com.simpleoffer;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;


@RunWith(Suite.class)
@Suite.SuiteClasses({
		ProductRepositoryTests.class,
		OfferRepositoryTests.class,
		OfferControllerIntegrationTests.class,
		ProductControllerIntegrationTests.class
})
public class SimpleOfferApplicationTests {

	@Test
	public void contextLoads() {
	}

}
